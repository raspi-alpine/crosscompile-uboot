#!/bin/sh
# Patch to suppress serial output according to
# https://raspberrypi.stackexchange.com/questions/116074/how-can-i-disable-the-serial-console-on-distributions-that-use-u-boot

[ ! -e "configs/$1" ] && echo "ERROR: configs/$1 not found" && exit 1

# Note, this may run for already patched rpi.h since pattern is only found once.
sed -i 's/^\([[:space:]]*\)\(ENV_DEVICE_SETTINGS\)/\1"silent=1\\0" \2/' include/configs/rpi.h

cat >>"configs/$1" <<EOF

CONFIG_BOOTDELAY=-2
CONFIG_SILENT_CONSOLE=y
CONFIG_SYS_DEVICE_NULLDEV=y
CONFIG_SILENT_CONSOLE_UPDATE_ON_SET=y
CONFIG_SILENT_U_BOOT_ONLY=y
EOF
