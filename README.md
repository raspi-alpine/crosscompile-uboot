# crosscompile-uboot

Build u-boot tarball with the latest version of u-boot for raspberry pi's, and upload to the package registry.  Checks to see if it is already built first.

Scheduled to run weekly to check for new versions.

| CI/Pipeline variables | Description |
| -------- | -------------------------------------------------- |
| BUILDVER | version to build eg 2021.10 , if not already built. If unset latest version is used |
| BUILDIT  | set to force build even if built already           |
